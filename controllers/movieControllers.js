const { userprofile } = require('../models');
const { movie } = require('../models');
//const { historytraining } = require('../models');
const Sequelize = require('sequelize');

// const { Op } = require("sequelize");
const { QueryTypes } = require('sequelize');
// const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('miniproject', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
  });

exports.getSearchMovie = async (req, res) => {
    try {
        await sequelize.authenticate();
        const querygetMoviesByTitle = ("SELECT count(`userprofiles`.`review_id`) as `countReview`,`movies`.`movie_id` AS `movies.movie_id`, `movies`.`title` AS `movies.title`, `movies`.`year` AS `movies.year`,`movies`.`genre` AS `movies.genre`, `movies`.`url_cover_image` AS `movies.url_cover_image`,`movies`.`url_trailer` AS `movies.url_trailer` FROM `movies` LEFT JOIN `userprofiles` ON `userprofiles`.`movie_id` = `movies`.`movie_id` WHERE `movies`.`title` LIKE '%" + req.body.title + "%' GROUP BY `movies`.`movie_id`")
        
        const data = await sequelize.query(querygetMoviesByTitle, { type: QueryTypes.SELECT });
        if (data === null) {
            res.status(201).json({
                status: false,
                message: 'movie not found',
                data: null,
            })
        }
        else {
            // let listMovies = [];
            // for(index = data.length - 1; index >= 0; --index) 
            //     {
            //         listMovies[index] = { 
            //             review_id: data[index].countReview,
            //             movie_id: data[index].movie_id,
            //             title: data[index].title,
            //             year: data[index].year,
            //             genre: data[index].genre,
            //             descirption: data[index].descirption,
            //             url_trailer: data[index].url_trailer,
            //             url_cover_image: data[index].url_cover_image,
            //         }
            //     }
            // try {
            //     sequelize.close();
            //     console.log('Connection has been established successfully.');
                res.status(200).json({
                    status: true,
                    message: `movies with keyword search ${req.body.title} retrieved!`,
                    data,
                    querygetMoviesByTitle
                })
            // } 
            // catch (err) {
            //     res.status(422).json({
            //         status: false,
            //         message: err.message,
            //         data: null,
            //     })
            // }
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message,
            data: null,
        })
    }
}