const { user } = require('../models');
const JWT = require('jsonwebtoken');
const bcrypt = require('bcrypt');
//const { historytraining } = require('../models');
const { Op } = require("sequelize");


exports.getMyProfileUserByEmail = async (req, res) => {
    try {
        const data = await user.findOne({ 
            where: { 
              email: req.user.email,
            }
        });
        if (data === null) {
            res.status(201).json({
                status: false,
                message: 'user not found',
                data: null,
            })
        }
        else {
            res.status(200).json({
                status: true,
                message: `users with email ${req.body.email} retrieved!`,
                data: {
                    name: data.name,
                    username: data.username,
                    email: data.email,
                    password: data.password,
                    profile_picture: data.profile_picture,
                }
            })
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message,
            data: req.user.email + "test",
        })
    }
}

exports.getMyProfileUserByID = async (req, res) => {
    try {
        const data = await user.findOne({ 
            where: { 
              user_id: req.user.user_id,
            }
        });
        if (data === null) {
            res.status(201).json({
                status: false,
                message: 'user not found',
                data: null,
            })
        }
        else {
            res.status(200).json({
                status: true,
                message: `users with email ${req.body.email} retrieved!`,
                data: {
                    name: data.name,
                    username: data.username,
                    email: data.email,
                    password: data.password,
                    profile_picture: data.profile_picture,
                }
            })
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}


exports.insertUser = async (req, res) => {
    try {
        const findEmailUser = await user.findOne({ 
            where: { 
                [Op.or]: [{  email: req.body.email }, { username: req.body.email }], 
            }
        });
        if (findEmailUser != null) {
            res.status(201).json({
                status: false,
                message: 'Can not create users because email / username already exist in other user!',
                data: null,
            })
        } 
        else {
            try {
                const iscreate = await user.create({
                    username: req.body.username,
                    email: req.body.email,
                    password: req.body.password,
                    name: req.body.name
                })
                if (iscreate) {
                    try {
                        const data = await user.findOne({ 
                            where: { 
                            email: req.body.email, 
                            }
                        });
                        if (data) {
                            const token = JWT.sign(
                                { 
                                    user : { 
                                        user_id: data.user_id,
                                        email: data.email 
                                    }
                                },
                                  process.env.SECRETKEY
                                );
                            res.status(201).json({
                                status: true,
                                message: 'user created!!',
                                data: {
                                    token,
                                    name: data.name,
                                    username: data.username,
                                    email: data.email,
                                    password: data.password,
                                    profile_picture: data.profile_picture,
                                }
                            })
                        }
                        else {
                            res.status(201).json({
                                status: false,
                                message: 'can not registered!!',
                                data: null,
                            })
                        }
                    }
                    catch(error) {

                    }
                }    
                else {
                    res.status(201).json({
                        status: false,
                        message: 'can not registered!!',
                        data: null,
                    })
                }    
            }
            catch(err) {
                res.status(422).json({
                    status: false,
                    message: err.message,
                    data: null,
                })
            }
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message,
            data: null,
        })
    }
}

exports.updateUser = async (req, res) => {
    try {
        const data = await user.findOne({ 
            where: { 
                username: req.body.username, 
            }
        });
        if ((data != null) || (req.data == data.username && req.email == data.email)) {
            try {
                let dataupdate = await user.update(
                    {
                        password: (req.body.password == null || req.body.password == "") ? data.password : req.body.password,
                        name: (req.body.name == null || req.body.name == "") ? data.name : req.body.name
                    }, 
                    {  
                        where: {username: req.body.username}
                    }
                )
                if (dataupdate != 0) {
                    res.status(201).json({
                        status: true,
                        message: 'user updated!!',
                        data: {
                            name: data.name,
                            username: data.username,
                            email: data.email,
                            password: data.password,
                            profile_picture: data.profile_picture,
                        }
                    })
                }
                else {
                    res.status(201).json({
                        status: false,
                        message: 'can not updated!!',
                        data: null,
                    })
                }
            }
            catch(error) {
                res.status(201).json({
                    status: false,
                    message: 'can not updated!!',
                    data: null,
                })
            }
        } 
        else {
            res.status(422).json({
                status: false,
                message: err.message,
                data: null,
            })
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message,
            data: null,
        })
    }
}


exports.deleteUser = async (req, res) => {
    try { 
        let datadelete = await user.destroy({where: {id: req.params.id}});
        if ( datadelete == 0) {
            res.status(202).json({
                status: false,
                message: `user with id ${req.params.id} not found!`,
                data: null,
            })
        }
        else {
            res.status(200).json({
                status: true,
                message: `${n} rows executed!! user with id ${req.params.id} deleted!`,
                data: null,
            })
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

exports.login = async (req, res) => {
    try {
        const findEmailUser = await user.findOne({ 
            where: { 
              email: req.body.email, 
            }
        });
        if (findEmailUser == null) {
            res.status(201).json({
                status: false,
                message: 'email not found!',
                data: null,
            })
        } 
        else if (req.headers.api_key != process.env.API_KEY) {
            res.status(201).json({
                status: false,
                message: 'Login Failed!',
                data: null,
            })
        }
        else {
            try {
                const passwordUser = findEmailUser.password;
                const passwordUserinput = req.body.password
                const comparePassword = bcrypt.compareSync(passwordUserinput, passwordUser);
                if (!comparePassword) {
                    res.status(401).json({
                      status: false,
                      message: "Login failed !!",
                      data: null,
                    })
                  } 
                else {
                    const token = JWT.sign(
                        { 
                            user : { 
                                user_id: findEmailUser.user_id,
                                email: findEmailUser.email 
                            } 
                        },
                          process.env.SECRETKEY
                        );
                    res.status(201).json({
                        status: true,
                        message: "your email is right",
                        data: {
                            token,
                            name: findEmailUser.name,
                            username: findEmailUser.username,
                            email: findEmailUser.email,
                            password: findEmailUser.password,
                            profile_picture: findEmailUser.profile_picture,
                        }
                    })
                }
            }
            catch(err) {
                res.status(422).json({
                    status: false,
                    message: "error jwt " + err.message,
                    data: null,
                })
            }
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: req.body.email + "error find email " +  err.message,
            data: null,
        })
    }
}