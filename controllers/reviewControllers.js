const { userprofile } = require('../models');
const { review } = require('../models');
const { user } = require('../models');
const { movie } = require('../models');
//const { historytraining } = require('../models');
const { Op } = require("sequelize");


exports.getMyReviews = async (req, res) => {
    try {
        const data = await userprofile.findAll({ 
            attributes: ["review_id","movie_id","user_id"],
            include: [ 
                {
                    model: movie,
                    as: 'movie',
                    attributes: ["title","url_cover_image"],
                },
                {
                    model: review,
                    as: 'review',
                    attributes: ["headline","review","rating","review_at"],
                },
                {
                    model: user,
                    as: 'user',
                    attributes: ["email"],
                },
            ],
            where: {user_id: req.user.user_id},
        });
        if (data === null) {
            res.status(201).json({
                status: false,
                message: 'user not found',
                data: null,
            })
        }
        else {
            let listreview = [];
            for(index = data.length - 1; index >= 0; --index) 
                {
                    listreview[index] = { 
                        review_id: data[index].review_id,
                        headline: data[index].review.headline,
                        review: data[index].review.review,
                        rating: data[index].review.rating,
                        review_at: data[index].review.review_at,
                        movie_id: data[index].movie_id,
                        title: data[index].movie.title,
                        url_cover_image: data[index].movie.url_cover_image,
                        reviewer: data[index].user.email,
                    }
                }
            res.status(200).json({
                status: true,
                message: `reviews with email ${req.user.email} retrieved!`,
                // data: {
                    // review_id: data.review_id,
                    // headline: data.review.headline,
                    // review: data.review.review,
                    // rating: data.review.rating,
                    // review_at: data.review.review_at,
                    // movie_id: data.movie_id,
                    // title: data.movie.title,
                    // url_cover_image: data.movie.url_cover_image,
                    // reviewer: data.email,
                // }
                 data: listreview,
            })
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message,
            data: null,
        })
    }
}