'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class movie extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.belongsToMany(models.userprofile, {
        through: "userprofiles",
        foreignKey: "movie_id",
        as: "moviesuserprofiles"
      })

      // this.belongsToMany(models.user, {
      //   through: "userprofiles",
      //   foreignKey: "movie_id",
      //   as: "usermovies"
      // })

      this.belongsToMany(models.review, {
        through: "userprofiles",
        foreignKey: "movie_id",
        as: "moviereviews"
      })

    }
  };
  movie.init({
    movie_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    title: DataTypes.STRING,
    year: DataTypes.STRING,
    description: DataTypes.TEXT,
    cover: DataTypes.STRING,
    genre: DataTypes.STRING,
    url_cover_image: DataTypes.STRING,
    url_trailer: DataTypes.STRING,
    rating: DataTypes.FLOAT
  }, {
    sequelize,
    modelName: 'movie',
  });
  return movie;
};