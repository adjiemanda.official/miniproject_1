'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userprofile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.review, { foreignKey: "review_id", as: "review" });
      this.belongsTo(models.user, { foreignKey: "user_id", as: "user" });
      this.belongsTo(models.movie, { foreignKey: "movie_id", as: "movie" });
    }
  };
  userprofile.init({
    user_id: DataTypes.INTEGER,
    movie_id: DataTypes.INTEGER,
    review_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'userprofile',
  });
  return userprofile;
};