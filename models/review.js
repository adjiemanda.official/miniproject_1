'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class review extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.belongsToMany(models.userprofile, {
        through: "userprofiles",
        foreignKey: "review_id",
        as: "reviewsuserprofiles"
      })

      // this.belongsToMany(models.user, {
      //   through: "userprofiles",
      //   foreignKey: "review_id",
      //   as: "userreviews"
      // })

      this.belongsToMany(models.movie, {
        through: "userprofiles",
        foreignKey: "review_id",
        as: "moviereviews"
      })
      

    }
  };
  review.init({
    review_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    headline: DataTypes.STRING,
    review: DataTypes.STRING,
    rating: DataTypes.INTEGER,
    review_at: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'review',
  });
  return review;
};