'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.belongsToMany(models.userprofile, {
        through: "userprofiles",
        foreignKey: "user_id",
        as: "usersuserprofiles"
      })

      // this.belongsToMany(models.movie, {
      //   through: "userprofiles",
      //   foreignKey: "user_id",
      //   as: "usermovies"
      // })

      // this.belongsToMany(models.review, {
      //   through: "userprofiles",
      //   foreignKey: "user_id",
      //   as: "userreviews"
      // })

    }
  };
  user.init({
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    profile_picture: DataTypes.STRING,
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user',
  });
  return user;
};