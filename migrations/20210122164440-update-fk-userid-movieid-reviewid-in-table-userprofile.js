'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.changeColumn( 
      'userprofiles', 
      'user_id',
      {
       type : Sequelize.INTEGER,
       references: {
         model: 'users',
         key: 'user_id',
       }
      });
 
     await queryInterface.changeColumn( 
       'userprofiles', 
       'movie_id',
       {
        type : Sequelize.INTEGER,
        references: {
          model: 'movies',
          key: 'movie_id',
        }
       });

      await queryInterface.changeColumn( 
      'userprofiles', 
      'review_id',
      {
        type : Sequelize.INTEGER,
        references: {
          model: 'reviews',
          key: 'review_id',
        }
      });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.changeColumn('userprofiles','user_id',{type : Sequelize.INTEGER});
    await queryInterface.changeColumn('userprofiles','movie_id',{type : Sequelize.INTEGER});
    await queryInterface.changeColumn('userprofiles','review_id',{type : Sequelize.INTEGER});
  }
};
