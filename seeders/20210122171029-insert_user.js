'use strict';
const bcrypt = require('bcrypt');
const pass = 'Test1234'
const saltRounds = 10;
const passdefault = bcrypt.hashSync(pass, saltRounds);

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

  await queryInterface.bulkInsert('users', 
    [
      {
        username: 'chinmi',
        email: 'chinmi@test.com',
        password: passdefault,
        name: 'Chinmi keren',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'rahmanda',
        email: 'rahmanda@test.com',
        password: passdefault,
        name: 'Rahmanda Fajri',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'adjie',
        email: 'adjie@test.com',
        password: passdefault,
        name: 'adjie rahmanda',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'manda',
        email: 'manda@test.com',
        password: passdefault,
        name: 'manda adjie',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('users', null, {});
  }
};
