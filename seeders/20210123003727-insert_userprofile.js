'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return queryInterface.bulkInsert('userprofiles', [
      {
        user_id: '1',
        movie_id: '1',
        review_id: '1',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: '2',
        movie_id: '1',
        review_id: '2',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: '3',
        movie_id: '1',
        review_id: '3',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: '4',
        movie_id: '1',
        review_id: '4',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: '1',
        movie_id: '2',
        review_id: '5',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: '2',
        movie_id: '2',
        review_id: '6',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: '3',
        movie_id: '2',
        review_id: '7',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: '4',
        movie_id: '2',
        review_id: '8',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: '1',
        movie_id: '3',
        review_id: '9',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: '2',
        movie_id: '3',
        review_id: '1',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: '3',
        movie_id: '3',
        review_id: '2',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: '4',
        movie_id: '3',
        review_id: '3',
        createdAt: new Date(),
        updatedAt: new Date(),
      }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('userprofiles', null, {});
  }
};
