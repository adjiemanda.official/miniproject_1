'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('movies', 
   [
     {
      title: "Halloween",
      year: "1978",
      genre: "Horror",
      description: "On a cold Halloween night in 1963, six year.old Michael Myers brutally murdered his 17 year old sister, Judith. He was sentenced and locked away inside a sanitarium under the care of child psychiatrist Dr. Sam Loomis. On October 30, 1978, Myers escapes and makes his way back home to Haddonfield, turning a night of tricks and treats into something much more sinister for three young women, including Laurie Strode, the star making role for Jamie Lee Curtis",
      url_cover_image: "https://www.movienewsletters.net/photos/006822R1.jpg", 
      url_trailer: "https://youtu.be/T5ke9IPTIJQ",
      rating: "0.0",
      genre: 'Action',
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      title: "Nightmare on Elm Street",
      year: "1984",
      genre: "Horror",
      description: "The opposite of Michael Myers in almost every way, Freddy Krueger is chatty and expressive. As the sequels go on, he gets more and more ridiculous, but in the original Nightmare on Elm Street, he was appropriately menacing with a burned visage based on cheese pizza. The child molester and murderer, set free due to a technicality, was killed by the mob justice of local parents. But when the undead Freddy reaches their children through dreams, continuing his rampage under cover of dark, the joke is on them.",
      url_cover_image: "https://www.imdb.com/title/tt1179056/mediaviewer/rm1111920384?ref_=tt_ov_i", 
      url_trailer: "https://youtu.be/dCVh4lBfW-c",
      rating: "0.0",
      genre: 'Horror',
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      title: "The Exorcist",
      year: "1973",
      genre: "Horror",
      description: "The Exorcist is considered by many to be the scariest film ever made. Based on William Peter Blatty's novel of the same name, the film follows a possessed child, and the dual priest team sent to exorcise her. The film received ten Academy Award nominations, including Best Picture, marking it as the first (and still one of the few) horror films to be nominated for Best Picture. The intense acting and terrifying special effects created such an effective film that the production itself was rumored to be cursed. Nearly everyone associated with the film was injured, fell ill, or suffered a death in the family - and the whole set almost burned to the ground. Where'd we leave the holy water?",
      url_cover_image: "https://www.imdb.com/title/tt0070047/mediaviewer/rm1202605312?ref_=tt_ov_i", 
      url_trailer: "https://youtu.be/a8g3h2JaVE4",
      rating: "0.0",
      genre: 'Horror',
      createdAt: new Date(),
      updatedAt: new Date()
     },
   ]
   );
    
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('movies', null, {});
  }
};
