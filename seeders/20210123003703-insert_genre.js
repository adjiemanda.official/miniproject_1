'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('genres', 
    [
      {
        genre: 'Action',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        genre: 'Horror',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        genre: 'Drama',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        genre: 'Comedy',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        genre: 'Romance',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        genre: 'Thriller',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        genre: 'Adventure',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        genre: 'Mysteri',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('genres', null, {});
  }
};
