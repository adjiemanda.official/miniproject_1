'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

   await queryInterface.bulkInsert('reviews', 
   [
     {
      headline: "Mantab",
      review: "filmnya bagus banget",
      rating: "10",
      review_at: new Date(),
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      headline: "keren",
      review: "filmnya keren banget",
      rating: "9",
      review_at: new Date(),
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      headline: "sukakk",
      review: "filmnya sukakk banget",
      rating: "10",
      review_at: new Date(),
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      headline: "jelek",
      review: "filmnya jelek banget",
      rating: "3",
      review_at: new Date(),
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      headline: "payah",
      review: "filmnya payah banget",
      rating: "4",
      review_at: new Date(),
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      headline: "Mantab",
      review: "filmnya mantab banget",
      rating: "9",
      review_at: new Date(),
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      headline: "keren",
      review: "filmnya keren banget",
      rating: "8",
      review_at: new Date(),
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      headline: "jelek",
      review: "filmnya jelek banget",
      rating: "5",
      review_at: new Date(),
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      headline: "ngga suka",
      review: "filmnya jelek banget",
      rating: "2",
      review_at: new Date(),
      createdAt: new Date(),
      updatedAt: new Date()
     },
   ]
   );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
