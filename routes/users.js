var express = require('express');
var router = express.Router();

const userControllers = require('../controllers/userControllers');
const passport = require('passport');



/* GET my user profile  by Email */
router.get('/',passport.authenticate('jwt', { 'session': false }), userControllers.getMyProfileUserByEmail);

/* GET my user profile by ID */
router.get('/get/ByID',passport.authenticate('jwt', { 'session': false }), userControllers.getMyProfileUserByID);


/* POST my user profile register. */
router.post('/', userControllers.insertUser);

/* PUT my user profile updating. */
router.put('/',passport.authenticate('jwt', { 'session': false }), userControllers.updateUser);


/* DELETE my user profile deleting. */
router.delete('/',passport.authenticate('jwt', { 'session': false }), userControllers.deleteUser);

/* POST my user profile login. */
router.post('/api/login', userControllers.login);

module.exports = router;
