var express = require('express');
var router = express.Router();

const  movieControllers = require('../controllers/movieControllers');
const passport = require('passport');

/* GET movie search by movie name */
router.get('/',passport.authenticate('jwt', { 'session': false }), movieControllers.getSearchMovie);


module.exports = router;