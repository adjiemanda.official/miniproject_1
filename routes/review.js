var express = require('express');
var router = express.Router();

const reviewControllers = require('../controllers/reviewControllers');
const passport = require('passport');

/* GET my user profile  by Email */
router.get('/',passport.authenticate('jwt', { 'session': false }), reviewControllers.getMyReviews);


module.exports = router;